$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/AddBookToBasket.feature");
formatter.feature({
  "name": "User should be able to add a book to the basket and verify the correct book is added",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "As a customer, I want to buy a book",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.scenario({
  "name": "Place an order for the required product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Customer navigates to the website",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDef.navigateToWebsite()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Customer searches for book",
  "keyword": "When "
});
formatter.match({
  "location": "StepDef.searchForBook()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Customer add book to cart",
  "keyword": "And "
});
formatter.match({
  "location": "StepDef.addBookToCart()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Customer is redirected to cart page",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef.redirectionToCartPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Customer verifies the correct book is added",
  "keyword": "And "
});
formatter.match({
  "location": "StepDef.verifyBookIsAdded()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});