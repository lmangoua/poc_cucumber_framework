#Author: lionel.mangoua
#Date: 24/07/20

Feature: User should be able to add a book to the basket and verify the correct book is added

  Background: As a customer, I want to buy a book
  Scenario: Place an order for the required product
    Given Customer navigates to the website
    When Customer searches for book
    And Customer add book to cart
    Then Customer is redirected to cart page
    And Customer verifies the correct book is added