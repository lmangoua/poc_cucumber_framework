package stepdefs;

/**
 * @author lionel.mangoua
 * date: 24/07/20
 */

import Engine.Hook;
import implementation.AddBookToBasket;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utils.PropertyFileReader;

public class StepDef {

    Hook hook = new Hook();
    AddBookToBasket addBook = new AddBookToBasket();
    PropertyFileReader property = new PropertyFileReader();

    String url = property.returnPropVal_web(hook.web_fileName, "url");
    String bookTitle = property.returnPropVal_web(hook.web_fileName, "bookTitle");

    @Given("Customer navigates to the website")
    public void navigateToWebsite() {

        addBook.navigateToUrl(url);
    }

    @When("Customer searches for book")
    public void searchForBook() {

        addBook.searchBook(bookTitle);
    }

    @And("Customer add book to cart")
    public void addBookToCart() {

        addBook.addToCart(bookTitle);
    }

    @Then("Customer is redirected to cart page")
    public void redirectionToCartPage() {

        addBook.validateCartPage();
    }

    @And("Customer verifies the correct book is added")
    public void verifyBookIsAdded() {

        addBook.validateBookLabel(bookTitle);
    }
}