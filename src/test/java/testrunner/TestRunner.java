package testrunner;

/**
 * @author lionel.mangoua
 * date: 24/07/20
 */

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/test-output"},
        glue = {"stepdefs", "Engine"},
        features = {"src/test/resources/features/AddBookToBasket.feature"})

public class TestRunner {

    // TODO
}