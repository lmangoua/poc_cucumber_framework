package implementation;

/**
 * @author lionel.mangoua
 * date: 24/07/20
 */

import Engine.Hook;
import pageObjects.HomePage;
import utils.SeleniumUtility;

public class AddBookToBasket extends Hook {

    //region <navigateToUrl>
    public static void navigateToUrl(String url) {

        driver.get(url);

        SeleniumUtility.waitForElementByXpath(HomePage.amazonLogoIconXpath(), "Failed to wait for 'Amazon' logo");

        log("Navigated to " + url + " successfully","INFO",  "text");
    }
    //endregion

    //region <searchBook>
    public static void searchBook(String bookName) {

        //enter book title
        SeleniumUtility.enterTextByXpath(HomePage.searchBarXpath(), bookName, "Failed to enter 'Book Title'");

        //click search button
        SeleniumUtility.clickElementByXpath(HomePage.searchButtonXpath(), "Failed to click 'Search' button");

        //validate the book is found
        SeleniumUtility.waitForElementByXpath(HomePage.searchedBookTitleXpath(bookName), "Failed to wait for 'Searched Book Title'");

        log("Searched the Book '" + bookName + "' successfully","INFO",  "text");
    }
    //endregion

    //region <addToCart>
    public static void addToCart(String bookName) {

        //click book title
        SeleniumUtility.clickElementByXpath(HomePage.searchedBookTitleXpath(bookName), "Failed to click 'Searched Book Title'");

        //validate book info page
        SeleniumUtility.waitForElementByXpath(HomePage.bookTitleInfoPageXpath(bookName), "Failed to wait for 'Searched Book Title' on Book info page");

        //click 'Add to Cart'
        SeleniumUtility.clickElementByXpath(HomePage.addToCartButtonXpath(), "Failed to click 'Add to Cart' button");

        log("Added Book to Cart successfully","INFO",  "text");
    }
    //endregion

    //region <validateCartPage>
    public static void validateCartPage() {

        //validate 'Cart' button
        SeleniumUtility.waitForElementByXpath(HomePage.cartButtonXpath(), "Failed to wait for 'Cart' button");

        //validate 'Proceed to checkout (1 item)' button
        SeleniumUtility.waitForElementByXpath(HomePage.proceedToCheckoutButtonXpath(), "Failed to wait for 'Proceed to checkout (1 item)' button");

        //validate 'Added to Cart' label
        SeleniumUtility.waitForElementByXpath(HomePage.addToCartLabelXpath(), "Failed to wait for 'Added to Cart' label");

        log("Validated Cart Page successfully","INFO",  "text");
    }
    //endregion

    //region <validateBookLabel>
    public static void validateBookLabel(String bookName) {

        //validate 'Customers who shopped for The Marathon Continues: ' label
        SeleniumUtility.waitForElementByXpath(HomePage.customerWhoShoppedLabelXpath(bookName), "Failed to wait for 'Customers who shopped for The Marathon Continues: ' label");

        log("Validated Book is Added to Cart successfully","INFO",  "text");
    }
    //endregion
}
