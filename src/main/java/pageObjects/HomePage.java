package pageObjects;

/**
 * @author lionel.mangoua
 * date: 25/07/20
 */

public class HomePage {

    public static String amazonLogoIconXpath() {
        return "//span[@class='nav-sprite nav-logo-base']";
    }

    public static String searchBarXpath() {
        return "//input[@id = 'twotabsearchtextbox']";
    }

    public static String searchButtonXpath() {
        return "//input[@value = 'Go']";
    }

    public static String searchedBookTitleXpath(String bookTitle) {
        return "//a/span[contains(text(), '" + bookTitle + "')]";
    }

    public static String bookTitleInfoPageXpath(String bookTitle) {
        return "//h1/span[contains(text(), '" + bookTitle + "')]";
    }

    public static String addToCartButtonXpath() {
        return "//input[@id = 'add-to-cart-button']";
    }

    public static String cartButtonXpath() {
        return "//span/a[contains(text(), 'Cart')]"; //--> //a/span[contains(text(), 'Cart')]
    }

    public static String proceedToCheckoutButtonXpath() {
        return "(//a[contains(text(), ' Proceed to checkout (1 item)')])[1]";
    }

    public static String addToCartLabelXpath() {
        return "//h1[contains(text(), 'Added to Cart')]";
    }

    public static String customerWhoShoppedLabelXpath(String bookTitle) {
        return "//h2[contains(text(), 'Customers who shopped for ')]/i[@title = '" + bookTitle + "']";
    }
}
