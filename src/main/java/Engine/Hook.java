package Engine;

/**
 * @author lionel.mangoua
 * date: 25/07/20
 */

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.cucumber.core.logging.Logger;
import io.cucumber.core.logging.LoggerFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.PropertyFileReader;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class Hook {

    public static PropertyFileReader property = new PropertyFileReader();
    public static String web_fileName = "web";
    //Web
    public static RemoteWebDriver driver;
    public static String BROWSER = "Firefox"; //Firefox
    public static WebDriverWait wait = null;
    public static final int WAIT_TIME = 30;
    public static ChromeOptions options;
    public static String geckoPath = property.returnPropVal_web(web_fileName, "geckoPath");
    public static String chromePath = property.returnPropVal_web(web_fileName, "chromePath");
    public static String url = property.returnPropVal_web(web_fileName, "url");
    public static final Logger LOGGER = LoggerFactory.getLogger(Hook.class);

    @Before
    public static void initialiseDriverAndProperties() {

        setupLocalDriver();
    }

    //WEB
    //region <setupLocalDriver>
    public static void setupLocalDriver() {

        if("Firefox".equals(BROWSER)) {
            System.out.println("Web test is Starting ... \n");

            String geckoAbsolutePath;
            File geckoFile = new File(geckoPath);
            System.out.println("Gecko driver directory - " + geckoFile.getAbsolutePath());
            geckoAbsolutePath = geckoFile.getAbsolutePath();

            System.setProperty("webdriver.gecko.driver", geckoAbsolutePath + "");

            FirefoxOptions firefoxOptions = new FirefoxOptions();

            driver = new FirefoxDriver(firefoxOptions);
            System.out.println(BROWSER + " on local machine initiated \n");
        }
        else if("Chrome".equals(BROWSER)) {
            System.out.println("Web test is Starting ... \n");

            String chromeAbsolutePath;
            File chromeFile = new File(chromePath);
            System.out.println("Chrome driver directory - " + chromeFile.getAbsolutePath());
            chromeAbsolutePath = chromeFile.getAbsolutePath();

            System.setProperty("webdriver.chrome.driver", chromeAbsolutePath + "");

            options = new ChromeOptions();

            options.addArguments("--disable-extensions");
            options.addArguments("disable-infobars");
            options.addArguments("test-type");
            options.addArguments("enable-strict-powerful-feature-restrictions");
            options.setCapability(ChromeOptions.CAPABILITY, options);
            options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);

            driver = new ChromeDriver(options);
            System.out.println(BROWSER + " on local machine initiated \n");
        }

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, WAIT_TIME);
    }
    //endregion

    //region <logger>
    public static void logger(final String message, final String level, String format) {

        if(format.equalsIgnoreCase(("json"))) {
            String json = (new JSONObject(message)).toString(4); //To convert into pretty Json format
            LOGGER.info("\n" + json); //To print on the console
        }
        else {
            LOGGER.info(message); //To print on the console
        }
    }

    public static void log(final String message, final String level, String format) {

        try {
            logger(message, level, format);
        }
        catch (JSONException err) {
            logger(message, level, "text");
        }
    }
    //endregion

    @After
    //region <tearDown>
    public void tearDown() {

        try {
            if(driver != null) {
                //To check if the browser is opened
                System.out.println("Test - WEB is Ending ...\n");
                driver.quit();
            }
        }
        catch(Exception ex) {
            System.out.println("Something went wrong on test suite tear down!!! ---> " + ex);
        }
    }
    //endregion
}
